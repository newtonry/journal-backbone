JournalApp.Views.PostsIndex = Backbone.View.extend({
	template: JST['posts/index'],

	initialize: function() {
		this.listenTo(this.collection,
			"remove add change:title reset",
			this.render)
	},

	events: {
		"click .delete" : "deletePost"
	},

	render: function() {
		var renderedContent = this.template({
			posts: this.collection
		});

		this.$el.html(renderedContent);
		return this;
	},

	deletePost: function() {
		var targetId = parseInt($(event.target).attr("data-id"));
		var post = JournalApp.posts.findWhere({id: targetId});
		post.destroy();
	}



})