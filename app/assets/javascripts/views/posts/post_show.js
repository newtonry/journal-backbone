JournalApp.Views.PostShow = Backbone.View.extend({
	template: JST["posts/show"],

	events: {
		"dblclick .postTitle" : "editBox",
		"dblclick .postBody" : "editBox"

	},

	render: function() {
		var renderedContent = this.template({
			post: this.model
		});

		this.$el.html(renderedContent);
		return this;
	},

	editBox: function() {
		$(event.target).attr('contentEditable', true)	;
		// $(document).click('completeEdit');
	},
	//
	// completeEdit: function() {
	//
	//
	// }

});