JournalApp.Views.NewPost = Backbone.View.extend({
	template: JST["posts/new"],

	events: {
		"submit form" : "submitForm"

	},

	render: function () {
		var renderedContent = this.template();

		this.$el.html(renderedContent);
		return this;

	},

	submitForm: function(event) {
		event.preventDefault();

		var input = $(event.currentTarget).serializeJSON();
		var post = new JournalApp.Models.Post(input.post);

		post.save({}, {
			success: function () {
				JournalApp.posts.add(post);
				Backbone.history.navigate("/posts/" + post.id, {trigger: true});
			},

			error: function(response) {
				console.log('there was an error');
				console.log(response);

			}
		});

	}
});