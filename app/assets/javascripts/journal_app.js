//= require backbone
window.JournalApp = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {
    // alert('Hello from Backbone!');

		JournalApp.posts = new JournalApp.Collections.Posts();
		JournalApp.posts.fetch({
			success: function () {
				new JournalApp.AppRouter();
				Backbone.history.start();
			}
		});
  }
};

$(document).ready(function(){
  JournalApp.initialize();
});
