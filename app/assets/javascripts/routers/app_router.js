JournalApp.AppRouter = Backbone.Router.extend({
	routes: {
		"" : "showPostsIndex",
		"posts/new" : "showNewPost",
		"posts/:id" : "showPost",

	},

	showPostsIndex: function () {
		var indexView = new JournalApp.Views.PostsIndex({
			collection: JournalApp.posts
		});

		$('.sidebar').html(indexView.render().$el);
	},

	showPost: function (id) {
		var showView = new JournalApp.Views.PostShow({
			model: JournalApp.posts.get(id)
			});

			this.showPostsIndex();
			$('.content').html(showView.render().$el);

		},
	//
	showNewPost: function () {

		var newView = new JournalApp.Views.NewPost();

		this.showPostsIndex();
		$('.content').html(newView.render().$el);

		// var post = new JournalApp.Model.Post();
		// JournalApp.Models.Post(input.post)

		//
		// var newView = new JournalApp.Views.NewPost({
		// 	model: var post = new JournalApp.Model.Post()
		// });
		//
		// $('body').html(newView.render().$el);

	}

});